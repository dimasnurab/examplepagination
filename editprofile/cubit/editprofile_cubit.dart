import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:logger/logger.dart';
import '../config/networkexception.dart';
import '../config/validateException.dart';
import '../models/memberprofile_model.dart';
import '../repositories/editphoto_repo.dart';
import '../repositories/memberprofile_repo.dart';
import '../repositories/user_repo.dart';
import '../shared/firebasecrash_analis.dart';
import '../shared/lang/i18n.dart';
import '../shared/storage/memberProfile_storage.dart';
import '../shared/validators.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path/path.dart' as pathImage;
import 'package:mime/mime.dart';

import 'package:path_provider/path_provider.dart';
part 'editprofile_state.dart';

class EditprofileCubit extends Cubit<EditprofileState> {
  EditprofileCubit()
      : super(EditprofileState(dateTimeBirthDate: DateTime.now()));
  UserRepositories _repositories = UserRepositories();
  EditPhotoRepositories _editPhotoRepos = EditPhotoRepositories();

  MemberProfilerepositories _repos = MemberProfilerepositories();
  Validators _validators = Validators();

  getDatauser() async {
    final _name = await MemberProfileStorage.getName();
    final _email = await MemberProfileStorage.getEmail();
    final _phoneNumber = await MemberProfileStorage.getPhonenumber();
    final _birthPlace = await MemberProfileStorage.getbirthPlace();
    final _birthDate = await MemberProfileStorage.getbirthDate();
    final _parsetime = DateTime.parse(_birthDate);

    emit(state.update(
      name: _name,
      email: _email,
      phoneNumber: _phoneNumber,
      city: _birthPlace,
      birthDate: '${_parsetime.day}/${_parsetime.month}/${_parsetime.year}',
      dateTimeBirthDate: _parsetime,
    ));
  }

  changeName(String name) {
    emit(state.update(statusState: EditProfileStatusState.initial, name: name));
  }

  changeCity(String city) {
    emit(state.update(statusState: EditProfileStatusState.initial, city: city));
  }

  onChangeDateValue(dynamic dateTime) {
    var _dateResult = '${dateTime.day}/${dateTime.month}/${dateTime.year}';
    emit(state.update(
      statusState: EditProfileStatusState.initial,
      birthDate: _dateResult,
      dateTimeBirthDate: dateTime,
    ));
  }

  onSubmit(BuildContext context) async {
    emit(state.update(statusState: EditProfileStatusState.loading));
    try {
      final _isBirthPlace = await _validators.validateBirthPlace(state.city);
      final _isBirthDay =
          await _validators.validateRangeYears(state.dateTimeBirthDate);

      if (_isBirthPlace && _isBirthDay) {
        await _repositories.editProfile(state.name, state.email,
            state.dateTimeBirthDate, state.city, state.phoneNumber);

        // mengupdate local storage
        final _response = await _repos.fetchData();
        final _model = MemberProfileModel.fromJson(_response.data);
        await MemberProfileStorage.setJsonMember(_model.toJson());

        emit(state.update(statusState: EditProfileStatusState.done));
      }
    } on NetworkExceptionWithI18N catch (e) {
      emit(state.update(
          statusState: EditProfileStatusState.fail,
          errorMsg: I18N.text(context, e.responseMessage)));
    } on NetworkException catch (e) {
      emit(state.update(
          statusState: EditProfileStatusState.fail,
          errorMsg: e.responseMessage));
    } on ValidateException catch (e) {
      emit(state.update(
          statusState: EditProfileStatusState.fail,
          errorMsg: I18N.text(context, e.message)));
    } catch (e, s) {
      FirebaseCrashAnalist()
          .check(e: e, s: s, message: 'Error edit profile', isFatal: true);
      emit(state.update(
          statusState: EditProfileStatusState.fail, errorMsg: e.toString()));
    }
  }

  Future<void> uploadImage(BuildContext context, {required String path}) async {
    try {
      Directory _appDocDir = await getApplicationDocumentsDirectory();

      File _file = File(path);
      String _originalImage = pathImage.basename(path);
      if (_originalImage.contains('.jpg')) {
        File? _compressed = await FlutterImageCompress.compressAndGetFile(
          _file.absolute.path,
          "${_appDocDir.path}$_originalImage",
          quality: 95,
          minWidth: 1024,
          minHeight: 1024,
        );

        String mimeType = lookupMimeType(_compressed!.path)!;
        String basename = pathImage.basename(path);
        List<int> imageBytes = _compressed.readAsBytesSync();
        int fileSize = _compressed.lengthSync();
        //check sizesfoto
        final _sizeFoto = _checkSizesFoto(fileSize);
        emit(state.update(
          statusState: EditProfileStatusState.loadingUpload,
          sendProgress: 0,
          sizeFoto: _sizeFoto,
        ));

        final _response = await _editPhotoRepos.uploadImage(
          contentType: mimeType,
          name: basename,
          file: imageBytes,
          fileSize: fileSize,
          onSendProgress: (actualy, totalbytes) {
            var percentage = actualy / totalbytes * 100;
            emit(state.update(
              sendProgress: percentage,
            ));
          },
        );

        _updateImage(context, _response.data['id']);
      } else {
        emit(state.update(
            statusState: EditProfileStatusState.fail,
            errorMsg: 'File harus berupa jpg'));
      }
    } on NetworkExceptionWithI18N catch (e) {
      emit(state.update(
          statusState: EditProfileStatusState.fail,
          errorMsg: I18N.text(context, e.responseMessage)));
    } on NetworkException catch (e) {
      emit(state.update(
        statusState: EditProfileStatusState.fail,
        errorMsg: e.responseMessage,
      ));
    } catch (e, s) {
      emit(state.update(
        statusState: EditProfileStatusState.fail,
        errorMsg: e.toString(),
      ));
      FirebaseCrashAnalist()
          .check(e: e, s: s, message: 'Error edit profile', isFatal: true);
    }
  }

  _updateImage(BuildContext context, String id) async {
    try {
      final _response = await _editPhotoRepos.updateImage(id);
      Logger().d(_response.data);

      emit(state.update(
          statusState: EditProfileStatusState.uploadImageDone,
          photoUpdated: true));
    } on NetworkExceptionWithI18N catch (e) {
      emit(state.update(
          statusState: EditProfileStatusState.fail,
          errorMsg: I18N.text(context, e.responseMessage)));
    } on NetworkException catch (e) {
      emit(state.update(
        statusState: EditProfileStatusState.fail,
        errorMsg: e.responseMessage,
      ));
    } catch (e) {
      emit(state.update(
        statusState: EditProfileStatusState.fail,
        errorMsg: e.toString(),
      ));
    }
  }
}

_checkSizesFoto(int bytes) {
  var sizesFoto = ['B', 'KB', 'MB', 'GB', 'TB'];
  var order = 0;
  var len = bytes.toDouble();
  while (len >= 1024 && order++ < sizesFoto.length - 1) {
    len /= 1024;
  }
  return '${len.toStringAsFixed(2)} ${sizesFoto[order]}';
}
