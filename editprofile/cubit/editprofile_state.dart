part of 'editprofile_cubit.dart';

enum EditProfileStatusState {
  done,
  initial,
  fail,
  loading,
  uploadImageDone,
  loadingUpload
}

class EditprofileState extends Equatable {
  const EditprofileState({
    this.statusState = EditProfileStatusState.initial,
    this.errorMsg = '',
    this.email = '',
    this.name = '',
    this.city = '',
    this.birthDate = '00/00/0000',
    this.phoneNumber = '',
    this.sendProgress = 0,
    this.photoUpdated = false,
    this.sizeFoto = '',
    required this.dateTimeBirthDate,
  });

  final EditProfileStatusState statusState;
  final String errorMsg;
  final String email;
  final String name;
  final String city;
  final String birthDate;
  final String phoneNumber;
  final double sendProgress;
  final String sizeFoto;
  final DateTime dateTimeBirthDate;
  final bool photoUpdated;

  EditprofileState update({
    EditProfileStatusState? statusState,
    String? errorMsg,
    String? email,
    String? name,
    String? city,
    String? birthDate,
    String? phoneNumber,
    DateTime? dateTimeBirthDate,
    double? sendProgress,
    bool? photoUpdated,
    String? sizeFoto,
  }) =>
      copyWith(
        statusState: statusState,
        errorMsg: errorMsg,
        name: name,
        email: email,
        city: city,
        birthDate: birthDate,
        phoneNumber: phoneNumber,
        dateTimeBirthDate: dateTimeBirthDate,
        photoUpdated: photoUpdated,
        sendProgress: sendProgress,
        sizeFoto: sizeFoto,
      );

  EditprofileState copyWith({
    EditProfileStatusState? statusState,
    String? errorMsg,
    String? email,
    String? name,
    String? city,
    String? birthDate,
    String? phoneNumber,
    DateTime? dateTimeBirthDate,
    bool? photoUpdated,
    double? sendProgress,
    String? sizeFoto,
  }) =>
      EditprofileState(
        statusState: statusState ?? this.statusState,
        errorMsg: errorMsg ?? this.errorMsg,
        name: name ?? this.name,
        email: email ?? this.email,
        city: city ?? this.city,
        birthDate: birthDate ?? this.birthDate,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        dateTimeBirthDate: dateTimeBirthDate ?? this.dateTimeBirthDate,
        photoUpdated: photoUpdated ?? this.photoUpdated,
        sendProgress: sendProgress ?? this.sendProgress,
        sizeFoto: sizeFoto ?? this.sizeFoto,
      );

  @override
  List<Object> get props => [
        statusState,
        errorMsg,
        email,
        name,
        city,
        birthDate,
        dateTimeBirthDate,
        phoneNumber,
        photoUpdated,
        sizeFoto,
        sendProgress,
      ];
}
