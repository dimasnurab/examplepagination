import 'package:bloc/bloc.dart';

import 'package:equatable/equatable.dart';

import 'package:image_picker/image_picker.dart';
import '../shared/storage/memberProfile_storage.dart';

part 'editphoto_state.dart';

class EditphotoCubit extends Cubit<EditphotoState> {
  EditphotoCubit() : super(EditphotoState());

  ImagePicker _imagePicker = ImagePicker();

  initialGetData() async {
    final fotoMember = await MemberProfileStorage.getProfilePicture();
    emit(state.update(initialProfileId: fotoMember));
  }

  onImageButtonPick({required ImageSource source}) async {
    try {
      final _picked = await _imagePicker.getImage(
        source: source,
        maxHeight: 1024,
        maxWidth: 1024,
      );
      if (_picked != null) {
        emit(state.update(
            statusState: EditPhotoStatusState.done, file: _picked.path));
      }
    } catch (e) {
      emit(state.update(
        statusState: EditPhotoStatusState.fail,
        errorMsg: e.toString(),
      ));
    }
  }
}
