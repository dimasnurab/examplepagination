part of 'editphoto_cubit.dart';

enum EditPhotoStatusState { done, loading, initial, fail }

class EditphotoState extends Equatable {
  const EditphotoState({
    this.statusState = EditPhotoStatusState.initial,
    this.errorMsg = '',
    this.file = '',
    this.initialProfileId = '',
  });

  final EditPhotoStatusState statusState;
  final String errorMsg;
  final String file;
  final String initialProfileId;

  EditphotoState update({
    EditPhotoStatusState? statusState,
    String? errorMsg,
    String? file,
    String? initialProfileId,
  }) =>
      copyWith(
        statusState: statusState,
        errorMsg: errorMsg,
        file: file,
        initialProfileId: initialProfileId,
      );

  EditphotoState copyWith({
    EditPhotoStatusState? statusState,
    String? errorMsg,
    String? file,
    String? initialProfileId,
  }) =>
      EditphotoState(
        statusState: statusState ?? this.statusState,
        errorMsg: errorMsg ?? this.errorMsg,
        file: file ?? this.file,
        initialProfileId: initialProfileId ?? this.initialProfileId,
      );

  @override
  List<Object> get props => [statusState, errorMsg, file, initialProfileId];
}
