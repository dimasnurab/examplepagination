import 'dart:io';

import '../config/sizeconfig.dart';
import '../pages/profile/editprofile/cubit/editprofile_cubit.dart';
import '../themes/colors.dart';
import '../widgets/editphoto/cubit/editphoto_cubit.dart';
import '../widgets/imagenetwork_custom.dart';
import '../widgets/text_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';

import '../bottoast_custom.dart';

class EditPhotoView extends StatefulWidget {
  const EditPhotoView({
    Key? key,
    required this.photoUpdated,
  }) : super(key: key);
  final bool photoUpdated;
  @override
  _EditPhotoViewState createState() => _EditPhotoViewState();
}

class _EditPhotoViewState extends State<EditPhotoView> {
  late EditphotoCubit _cubit;
  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<EditphotoCubit>(context);
    _cubit.initialGetData();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BlocConsumer<EditphotoCubit, EditphotoState>(
      listener: (context, state) {
        if (state.statusState == EditPhotoStatusState.fail) {
          _showToast(state.errorMsg);
        }
        if (state.statusState == EditPhotoStatusState.done) {
          BlocProvider.of<EditprofileCubit>(context)
              .uploadImage(context, path: state.file);
        }
      },
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.only(top: getProportionateScreenWidth(20)),
          height: getProportionateScreenWidth(110),
          width: getProportionateScreenWidth(90),
          decoration: BoxDecoration(),
          child: Stack(
            children: [
              widget.photoUpdated
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: FileImage(File(
                                state.file,
                              )))),
                    )
                  : Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: ColorsApp.kRedStrong, shape: BoxShape.circle),
                      child: ImageNetworkCustom(
                        heightImage: MediaQuery.of(context).size.height,
                        widthImage: MediaQuery.of(context).size.width,
                        shape: BoxShape.circle,
                        imageId: state.initialProfileId,
                      ),
                    ),
              Positioned(
                bottom: 0,
                right: 0,
                child: GestureDetector(
                  onTap: _showChoosePickImage,
                  child: Container(
                    padding: EdgeInsets.all(getProportionateScreenWidth(2)),
                    decoration: BoxDecoration(
                        color: ColorsApp.kWhite,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(
                              getProportionateScreenWidth(4),
                            ),
                            topRight: Radius.circular(
                                getProportionateScreenWidth(4)))),
                    child: Icon(
                      Icons.camera_alt,
                      size: getProportionateScreenWidth(26),
                      color: ColorsApp.kblack,
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  _showToast(String message) {
    CustomBottoast().text(text: message, align: Alignment(0, 0.7));
  }

  _showChoosePickImage() {
    Get.bottomSheet(
      _BottomSheetPickImage(
        context: context,
        tapFromCamera: () {
          Get.back();
          _cubit.onImageButtonPick(source: ImageSource.camera);
        },
        tapFromGalery: () {
          Get.back();
          _cubit.onImageButtonPick(source: ImageSource.gallery);
        },
      ),
    );
  }
}

class _BottomSheetPickImage extends StatelessWidget {
  const _BottomSheetPickImage({
    Key? key,
    required this.context,
    this.tapFromCamera,
    this.tapFromGalery,
  }) : super(key: key);

  final BuildContext context;
  final Function()? tapFromCamera;
  final Function()? tapFromGalery;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getProportionateScreenWidth(160),
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: getProportionateScreenWidth(88),
            margin: EdgeInsets.only(
              bottom: 10,
            ),
            height: getProportionateScreenWidth(6),
            decoration: BoxDecoration(
                color: ColorsApp.kWhite,
                borderRadius:
                    BorderRadius.circular(getProportionateScreenWidth(10))),
          ),
          Container(
            margin: EdgeInsets.only(
                right: getProportionateScreenWidth(24),
                left: getProportionateScreenWidth(24),
                bottom: getProportionateScreenWidth(10)),
            padding: EdgeInsets.only(top: getProportionateScreenWidth(10)),
            height: getProportionateScreenWidth(100),
            decoration: BoxDecoration(
                color: ColorsApp.kWhite,
                borderRadius:
                    BorderRadius.circular(getProportionateScreenWidth(8))),
            child: Wrap(
              children: [
                _buildItem(context,
                    text: 'Camera roll',
                    icon: Icons.photo_album_rounded,
                    onTap: tapFromGalery),
                _buildItem(context,
                    text: 'Take photo',
                    icon: Icons.camera_alt,
                    onTap: tapFromCamera),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context,
      {required String text, required IconData icon, Function()? onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.only(left: getProportionateScreenWidth(16)),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: getProportionateScreenWidth(40),
                  width: getProportionateScreenWidth(60),
                  child: Icon(
                    icon,
                    color: ColorsApp.kgreyStrong,
                    semanticLabel: text,
                  ),
                ),
                TextCustom.all(
                  context,
                  text: text,
                  letterSpacing: 0,
                  fontWeight: FontWeight.w500,
                  colorText: ColorsApp.kLightBlack,
                  fontSize: 13.5,
                ),
              ],
            ),
            Divider(
              height: getProportionateScreenWidth(2),
              color: ColorsApp.kgreyMedium,
            )
          ],
        ),
      ),
    );
  }
}
