import '../config/sizeconfig.dart';
import '../pages/profile/editprofile/cubit/editprofile_cubit.dart';
import '../shared/hidekeyboard.dart';
import '../shared/lang/i18n.dart';
import '../themes/colors.dart';
import '../widgets/allerdialogCustom.dart';
import '../widgets/appbar_custom.dart';
import '../widgets/bottoast_custom.dart';
import '../widgets/custom_datepicker.dart';
import '../widgets/custom_formfield.dart';
import '../widgets/editphoto/cubit/editphoto_cubit.dart';
import '../widgets/outlinedbutton_custom.dart';
import '../widgets/stackwithprogress.dart';
import '../widgets/text_custom.dart';
import '../widgets/editphoto/widget_editphoto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  late EditprofileCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<EditprofileCubit>(context);
    _cubit.getDatauser();
  }

  _checkInput() {
    if (WidgetsBinding.instance!.window.viewInsets.bottom > 0.0) {
      //Keyboard is visible.
      KeyboardHider().hideKeyboardInput();
    } else {
      //Keyboard is not visible.
    }
  }

  @override
  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBarCustom(
        title: I18N.text(context, 'editProfile'),
      ),
      body: BlocConsumer<EditprofileCubit, EditprofileState>(
        listener: (context, state) {
          if (state.statusState == EditProfileStatusState.uploadImageDone) {
            _showToast('Photo has been update');
          }
          if (state.statusState == EditProfileStatusState.fail) {
            _showDialog(
                message: state.errorMsg,
                onTap: () => Navigator.of(context).pop());
          }
          if (state.statusState == EditProfileStatusState.done) {
            _showDialog(
              title: 'Berhasil',
              message: 'Data berhasil disimpan',
              onTap: () {
                Get.back();
                Get.back(result: 'BERHASIL');
              },
            );
          }
        },
        builder: (context, state) {
          return StackWithProgress(
            isLoading: state.statusState == EditProfileStatusState.loading,
            isLoadingUpload:
                state.statusState == EditProfileStatusState.loadingUpload,
            valueLoading: state.sendProgress,
            sizeFoto: state.sizeFoto,
            children: [
              GestureDetector(
                onTap: _checkInput,
                child: SingleChildScrollView(
                  child: Container(
                    height: getProportionateScreenWidth(540),
                    margin: EdgeInsets.only(
                        left: getProportionateScreenWidth(18),
                        right: getProportionateScreenWidth(18),
                        bottom: getProportionateScreenWidth(20)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                                child: BlocProvider(
                              create: (context) => EditphotoCubit(),
                              child: EditPhotoView(
                                  photoUpdated: state.photoUpdated),
                            )),
                            _buildNamefield(context, state),
                            _buildEmailField(context, state),
                            _buildRowBirthAndDatePicker(context, state),
                          ],
                        ),
                        outlinedButtonCustom(
                          context,
                          onPressed: () {
                            _cubit.onSubmit(context);
                          },
                          marginTop: 40,
                          textButton: I18N.text(context, 'simpan'),
                          height: 47,
                          fontSize: 14.5,
                          marginLeft: 18,
                          marginRight: 18,
                          colorText: ColorsApp.kWhite,
                          borderRadiusEnable: 4,
                          borderRadiusPressed: 4,
                          enableBgColor: ColorsApp.kRedStrong,
                          overlayColorPressed:
                              ColorsApp.kWhite.withOpacity(0.6),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Column _buildEmailField(BuildContext context, EditprofileState state) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextCustom.all(
          context,
          text: 'Email',
          fontWeight: FontWeight.w400,
          letterSpacing: 0,
          marginTop: 20,
          colorText: ColorsApp.kgreyStrong,
          fontSize: 13.5,
          marginLeft: 18,
        ),
        CustomFormField(
          marginLeft: 18,
          marginRight: 18,
          marginTop: 4,
          marginBot: 18,
          paddingLeft: 18,
          hintText: state.email,
          readOnly: true,
          backgroundColorField: Colors.transparent,
          textInputAction: TextInputAction.done,
          onChanged: (val) {},
          onSaved: (val) {},
        ),
      ],
    );
  }

  Column _buildNamefield(BuildContext context, EditprofileState state) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextCustom.all(
          context,
          text: I18N.text(context, 'nama'),
          letterSpacing: 0,
          fontWeight: FontWeight.w400,
          colorText: ColorsApp.kgreyStrong,
          fontSize: 13.5,
          marginLeft: 18,
          marginTop: 30,
        ),
        CustomFormField(
          marginLeft: 18,
          marginRight: 18,
          marginTop: 4,
          paddingLeft: 18,
          hintText: state.name,
          textInputAction: TextInputAction.done,
          onChanged: (val) {
            _cubit.changeName(val!);
          },
          onSaved: (val) {},
        ),
      ],
    );
  }

  Container _buildRowBirthAndDatePicker(
      BuildContext context, EditprofileState state) {
    SizeConfig().init(context);

    return Container(
      margin: EdgeInsets.only(
        left: getProportionateScreenWidth(18),
        right: getProportionateScreenWidth(18),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: [
          Flexible(
            child: _buildFormBirthPlace(context, state),
          ),
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextCustom.all(
                  context,
                  text: I18N.text(context, 'tanggalLahir'),
                  letterSpacing: 0,
                  fontWeight: FontWeight.w400,
                  colorText: ColorsApp.kgreyStrong,
                  fontSize: 13.5,
                  marginTop: 10,
                ),
                CustomDatePicker(
                  marginTop: 4,
                  resultDate: state.birthDate,
                  selectedDate: state.dateTimeBirthDate,
                  buttonConfirmStyleIOS:
                      Theme.of(context).textTheme.button?.copyWith(
                            fontSize: getProportionateScreenWidth(17),
                            color: Colors.black87,
                          ),
                  onChangeDate: (valDate) {
                    _cubit.onChangeDateValue(valDate);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFormBirthPlace(BuildContext context, EditprofileState state) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextCustom.all(
          context,
          text: I18N.text(context, 'tempatLahir'),
          letterSpacing: 0,
          fontWeight: FontWeight.w400,
          colorText: ColorsApp.kgreyStrong,
          fontSize: 13.5,
          marginTop: 10,
        ),
        CustomFormField(
          hintText: state.city,
          marginTop: 4,
          keyboardType: TextInputType.emailAddress,
          textCapitalization: TextCapitalization.characters,
          marginRight: getProportionateScreenWidth(6),
          paddingLeft: getProportionateScreenWidth(18),
          onChanged: (val) {
            _cubit.changeCity(val!);
          },
          onSaved: (val) {},
        ),
      ],
    );
  }

  _showDialog({
    String message = 'Error dialog',
    String title = 'Ooopss...',
    required Function() onTap,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => ErrorDialogCustom(
        title: title,
        message: message,
        tapButtonOne: onTap,
      ),
    );
  }

  _showToast(String message) {
    CustomBottoast()
        .text(text: message, align: Alignment(0.0, -0.8), color: Colors.green);
  }
}
