import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import '../config/networkexception.dart';
import '../models/notification_model.dart';
import '../repositories/notification_repo.dart';
import '../shared/firebasecrash_analis.dart';
import 'package:equatable/equatable.dart';
import '../shared/lang/i18n.dart';

part 'notification_state.dart';

class NotificationCubit extends Cubit<NotificationState> {
  NotificationCubit() : super(NotificationState());
  NotificationRepo _repo = NotificationRepo();

  getNotification(BuildContext context) async {
    emit(state.update(statusState: NotificationStatusState.loading));
    try {
      final response = await _repo.fetchData(offset: 0, limit: 10);
      final modelTenant = List<NotificationModel>.from(response.data['data']
          .map((json) => NotificationModel.fromJson(json)));

      emit(state.update(
        statusState: NotificationStatusState.done,
        model: modelTenant,
      ));
    } on NetworkExceptionWithI18N catch (e) {
      emit(state.update(
          statusState: NotificationStatusState.fail,
          errorMsg: I18N.text(context, e.responseMessage)));
    } on NetworkException catch (e) {
      emit(state.update(
          statusState: NotificationStatusState.fail,
          errorMsg: e.responseMessage));
    } catch (e, s) {
      FirebaseCrashAnalist()
          .check(e: e, s: s, message: 'Error Notification', isFatal: true);
      emit(state.update(
          statusState: NotificationStatusState.fail, errorMsg: e.toString()));
    }
  }

  fetchScrollData(BuildContext context) async {
    try {
      if (state.scrollStatusState != ScrollStatusState.fetchMAX) {
        emit(state.update(scrollStatusState: ScrollStatusState.loading));
        await Future.delayed(const Duration(milliseconds: 750));
        final response =
            await _repo.fetchData(offset: state.model.length, limit: 10);
        final modelTenant = List<NotificationModel>.from(response.data['data']
            .map((json) => NotificationModel.fromJson(json)));
        if (modelTenant.isEmpty || modelTenant.length < 0) {
          emit(state.update(
              scrollStatusState: ScrollStatusState.fetchMAX,
              errorMsg: 'Tidak ada data lagi'));
        }
        emit(state.update(
            model: state.model + modelTenant,
            scrollStatusState: ScrollStatusState.done));
      }
    } on NetworkExceptionWithI18N catch (e) {
      emit(state.update(
          scrollStatusState: ScrollStatusState.fail,
          errorMsg: I18N.text(context, e.responseMessage)));
    } on NetworkException catch (e) {
      emit(state.update(
          scrollStatusState: ScrollStatusState.fail,
          errorMsg: e.responseMessage));
    } catch (e, s) {
      FirebaseCrashAnalist()
          .check(e: e, s: s, message: 'Error Notification', isFatal: true);
      emit(
        state.update(
            scrollStatusState: ScrollStatusState.fail, errorMsg: e.toString()),
      );
    }
  }
}
