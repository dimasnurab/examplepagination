part of 'notification_cubit.dart';

enum NotificationStatusState {
  done,
  fail,
  initial,
  loading,
}
enum ScrollStatusState { done, fail, initial, loading, fetchMAX }

class NotificationState extends Equatable {
  final String errorMsg;
  final NotificationStatusState statusState;
  final List<NotificationModel> model;
  final ScrollStatusState scrollStatusState;

  const NotificationState({
    this.errorMsg = '',
    this.statusState = NotificationStatusState.initial,
    this.model = const <NotificationModel>[],
    this.scrollStatusState = ScrollStatusState.initial,
  });

  NotificationState update({
    String? errorMsg,
    NotificationStatusState? statusState,
    List<NotificationModel>? model,
    ScrollStatusState? scrollStatusState,
  }) =>
      copyWith(
        errorMsg: errorMsg,
        statusState: statusState,
        model: model,
        scrollStatusState: scrollStatusState,
      );

  NotificationState copyWith({
    String? errorMsg,
    List<NotificationModel>? model,
    NotificationStatusState? statusState,
    ScrollStatusState? scrollStatusState,
  }) =>
      NotificationState(
        errorMsg: errorMsg ?? this.errorMsg,
        statusState: statusState ?? this.statusState,
        scrollStatusState: scrollStatusState ?? this.scrollStatusState,
        model: model ?? this.model,
      );

  @override
  List<Object> get props => [errorMsg, statusState, scrollStatusState, model];
}
