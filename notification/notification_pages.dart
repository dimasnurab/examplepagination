import '../config/sizeconfig.dart';
import '../models/notification_model.dart';
import '../pages/notification/cubit/notification_cubit.dart';
import '../shared/lang/i18n.dart';
import '../themes/colors.dart';
import '../widgets/appbar_custom.dart';
import '../widgets/bottoast_custom.dart';
import '../widgets/card_notification.dart';
import '../widgets/shimmer_children.dart';
import '../widgets/stackwith_progress_shimmer.dart';
import '../widgets/text_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationsPage extends StatefulWidget {
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  late NotificationCubit _cubit;
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<NotificationCubit>(context);
    _scrollController.addListener(_onScroll);
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      _cubit.fetchScrollData(context);
    }
  }

  @override
  void dispose() {
    _cubit.close();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBarCustom(
        title: I18N.text(context, 'notifikasi'),
      ),
      body: BlocConsumer<NotificationCubit, NotificationState>(
        listener: (context, state) {
          if (state.scrollStatusState == ScrollStatusState.fetchMAX) {
            _showToast(
              I18N.text(context, "dataTelahDimuat"),
            );
          }
        },
        builder: (context, state) {
          return StackWithProgressShimmer(
            typeLoadingPage: TypeLoadingPage.notification,
            isLoading: state.statusState == NotificationStatusState.loading,
            children: [
              if (state.statusState == NotificationStatusState.done)
                ListView.builder(
                  key: PageStorageKey(widget.key),
                  controller: _scrollController,
                  itemCount: state.model.length + 1,
                  itemBuilder: (context, index) {
                    List<NotificationModel> _item = state.model;
                    if (index == state.model.length) {
                      return _buildLoadingScrollIndicator(state, context);
                    }
                    return CardNotification(
                      key: PageStorageKey("${_item[index].id}"),
                      title: _item[index].title,
                      description: _item[index].description,
                      date: _item[index].createdTime,
                      typeNotifikasi: _item[index].type,
                    );
                  },
                )
            ],
          );
        },
      ),
    );
  }

  Widget _buildLoadingScrollIndicator(
      NotificationState state, BuildContext context) {
    return Visibility(
      visible: state.scrollStatusState == ScrollStatusState.loading,
      child: Container(
        margin: EdgeInsets.symmetric(
            vertical: getProportionateScreenWidth(10),
            horizontal: getProportionateScreenWidth(120)),
        padding: EdgeInsets.symmetric(
            vertical: getProportionateScreenWidth(8),
            horizontal: getProportionateScreenWidth(8)),
        decoration: BoxDecoration(
            color: ColorsApp.kWhite,
            borderRadius: BorderRadius.circular(30),
            border: Border.all(color: ColorsApp.kLightBlack, width: 0.4)),
        child: TextCustom.all(
          context,
          text: I18N.text(context, "sedangMemuat"),
          fontSize: 12.5,
          fontWeight: FontWeight.w500,
          colorText: ColorsApp.kLightBlack,
          letterSpacing: 0.7,
        ),
      ),
    );
  }

  _showToast(String message) {
    CustomBottoast().text(text: message, align: Alignment(0, 0.7));
  }
}
